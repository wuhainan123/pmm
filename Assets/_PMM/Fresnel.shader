// Shader created with Shader Forge v1.38 
// Shader Forge (c) Neat Corporation / Joachim Holmer - http://www.acegikmo.com/shaderforge/
// Note: Manually altering this data may prevent you from opening it in Shader Forge
/*SF_DATA;ver:1.38;sub:START;pass:START;ps:flbk:,iptp:0,cusa:False,bamd:0,cgin:,lico:1,lgpr:1,limd:0,spmd:1,trmd:0,grmd:0,uamb:True,mssp:True,bkdf:False,hqlp:False,rprd:False,enco:False,rmgx:True,imps:True,rpth:0,vtps:0,hqsc:True,nrmq:1,nrsp:0,vomd:0,spxs:False,tesm:0,olmd:1,culm:0,bsrc:0,bdst:1,dpts:2,wrdp:True,dith:0,atcv:False,rfrpo:True,rfrpn:Refraction,coma:15,ufog:True,aust:True,igpj:False,qofs:0,qpre:1,rntp:1,fgom:False,fgoc:False,fgod:False,fgor:False,fgmd:0,fgcr:0.5,fgcg:0.5,fgcb:0.5,fgca:1,fgde:0.01,fgrn:0,fgrf:300,stcl:False,atwp:False,stva:128,stmr:255,stmw:255,stcp:6,stps:0,stfa:0,stfz:0,ofsf:0,ofsu:0,f2p0:False,fnsp:False,fnfb:False,fsmp:False;n:type:ShaderForge.SFN_Final,id:9361,x:33400,y:32712,varname:node_9361,prsc:2|custl-1015-OUT;n:type:ShaderForge.SFN_Fresnel,id:2182,x:32807,y:32754,varname:node_2182,prsc:2|NRM-5892-OUT,EXP-3362-OUT;n:type:ShaderForge.SFN_NormalVector,id:5892,x:32603,y:32692,prsc:2,pt:False;n:type:ShaderForge.SFN_Slider,id:3362,x:32547,y:32965,ptovrint:False,ptlb:node_3362,ptin:_node_3362,varname:node_3362,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,min:0,cur:1,max:10;n:type:ShaderForge.SFN_Color,id:1668,x:32678,y:33087,ptovrint:False,ptlb:node_1668,ptin:_node_1668,varname:node_1668,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,c1:0.1172414,c2:0,c3:1,c4:1;n:type:ShaderForge.SFN_Multiply,id:4105,x:32984,y:32882,varname:node_4105,prsc:2|A-2182-OUT,B-1668-RGB,C-3697-OUT;n:type:ShaderForge.SFN_Tex2d,id:7717,x:33008,y:32561,ptovrint:False,ptlb:node_7717,ptin:_node_7717,varname:node_7717,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,tex:b66bceaf0cc0ace4e9bdc92f14bba709,ntxv:0,isnm:False;n:type:ShaderForge.SFN_Add,id:1015,x:33166,y:32861,varname:node_1015,prsc:2|A-7717-RGB,B-4105-OUT;n:type:ShaderForge.SFN_Slider,id:3697,x:32887,y:33149,ptovrint:False,ptlb:node_3697,ptin:_node_3697,varname:node_3697,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,min:0,cur:1.870565,max:10;proporder:3362-1668-7717-3697;pass:END;sub:END;*/

Shader "Shader Forge/Fresnel" {
    Properties {
        _node_3362 ("node_3362", Range(0, 10)) = 1
        _node_1668 ("node_1668", Color) = (0.1172414,0,1,1)
        _node_7717 ("node_7717", 2D) = "white" {}
        _node_3697 ("node_3697", Range(0, 10)) = 1.870565
    }
    SubShader {
        Tags {
            "RenderType"="Opaque"
        }
        Pass {
            Name "FORWARD"
            Tags {
                "LightMode"="ForwardBase"
            }
            
            
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #define UNITY_PASS_FORWARDBASE
            #include "UnityCG.cginc"
            #pragma multi_compile_fwdbase_fullshadows
            #pragma multi_compile_fog
            #pragma only_renderers d3d9 d3d11 glcore gles 
            #pragma target 3.0
            uniform float _node_3362;
            uniform float4 _node_1668;
            uniform sampler2D _node_7717; uniform float4 _node_7717_ST;
            uniform float _node_3697;
            struct VertexInput {
                float4 vertex : POSITION;
                float3 normal : NORMAL;
                float2 texcoord0 : TEXCOORD0;
            };
            struct VertexOutput {
                float4 pos : SV_POSITION;
                float2 uv0 : TEXCOORD0;
                float4 posWorld : TEXCOORD1;
                float3 normalDir : TEXCOORD2;
                UNITY_FOG_COORDS(3)
            };
            VertexOutput vert (VertexInput v) {
                VertexOutput o = (VertexOutput)0;
                o.uv0 = v.texcoord0;
                o.normalDir = UnityObjectToWorldNormal(v.normal);
                o.posWorld = mul(unity_ObjectToWorld, v.vertex);
                o.pos = UnityObjectToClipPos( v.vertex );
                UNITY_TRANSFER_FOG(o,o.pos);
                return o;
            }
            float4 frag(VertexOutput i) : COLOR {
                i.normalDir = normalize(i.normalDir);
                float3 viewDirection = normalize(_WorldSpaceCameraPos.xyz - i.posWorld.xyz);
                float3 normalDirection = i.normalDir;
////// Lighting:
                float4 _node_7717_var = tex2D(_node_7717,TRANSFORM_TEX(i.uv0, _node_7717));
                float3 finalColor = (_node_7717_var.rgb+(pow(1.0-max(0,dot(i.normalDir, viewDirection)),_node_3362)*_node_1668.rgb*_node_3697));
                fixed4 finalRGBA = fixed4(finalColor,1);
                UNITY_APPLY_FOG(i.fogCoord, finalRGBA);
                return finalRGBA;
            }
            ENDCG
        }
    }
    FallBack "Diffuse"
    CustomEditor "ShaderForgeMaterialInspector"
}
