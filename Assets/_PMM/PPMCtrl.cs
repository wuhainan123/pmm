﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.Threading;
using System;
using System.Runtime.InteropServices;
using System.Reflection;

public class PPMCtrl : MonoBehaviour {
    public GameObject FailPanel;
    public GameObject Line;
    public GameObject Node;
    public Material Passed;
    public Material Failed;
    public Material Hide_Passed;
    public Material Hide_Failed;
    public Material HitMat;

    public Text text;

    public float DrawSpeed = 100;

    Dropdown ColumnTagNameDd;
    Dropdown StoryGroupNameDd;
    Dropdown sectionNameDd;
    Button refreshBtn;
    Button ScreenRefreshBtn;
    //Button generateBtn;

    Button ScreenBtn;
    Button SettingBtn;
    GameObject LineSelectPanel;
    GameObject ScreenPanel;

    Dropdown ColumnLineDd;
    Dropdown StoryDd;
    //Dropdown ElementldDd;
    Dropdown LoadCaseDd;

    public string RDPMMPath;
    public string SDPMMPath;


    [DllImport("Armadillo", CallingConvention = CallingConvention.Cdecl)]
    [return: MarshalAs(UnmanagedType.BStr)]
    public static extern string Initialise([MarshalAs(UnmanagedType.BStr)] string sProg, [MarshalAs(UnmanagedType.BStr)] string sVer, bool isTest = false);

    [DllImport("Armadillo", CallingConvention = CallingConvention.Cdecl)]
    [return: MarshalAs(UnmanagedType.BStr)]
    public static extern string Validate([MarshalAs(UnmanagedType.BStr)] string sRet);

    [DllImport("Armadillo", CallingConvention = CallingConvention.Cdecl)]
    [return: MarshalAs(UnmanagedType.Bool)]
    public static extern bool LogFeature([MarshalAs(UnmanagedType.BStr)] string sFeature);

    [DllImport("Armadillo", CallingConvention = CallingConvention.Cdecl)]
    [return: MarshalAs(UnmanagedType.I4)]
    public static extern int DaysSinceCheck();


    bool CheckLicense(string appName, string appVersion, bool isTest = false) {
        
        try
        {
            
            string sRet = Initialise(appName, appVersion, isTest);
            string b = Validate(sRet);
            if (appName == b)
            {
                return true;
            }
            return false;
        }
        catch (Exception e)
        {
            Debug.Log(e.Message);
            return false;
        }
    }

    float timer;
    void Start() {

        try
        {
            var isLicensed = CheckLicense("EA_DDT_PMMViewer", "1.0", true);
            Debug.Log("license:"+isLicensed);

            if (isLicensed == false) {
                FailPanel.SetActive(true);
                this.enabled = false;
                return;
            }
        }
        catch (Exception e)
        {
            FailPanel.SetActive(true);
            Debug.Log(e.Message);
            this.enabled = false;
            return;
        }

        ca = Camera.main;
        //RDPMMPath = Application.dataPath+ "/CSVs/RDPMMReults.csv";
        //SDPMMPath = Application.dataPath + "/CSVs/SDPMMData.csv";
        ColumnTagNameDd = GameObject.Find("ColumnTagNameDd").GetComponent<Dropdown>();
        StoryGroupNameDd = GameObject.Find("StoryGroupNameDd").GetComponent<Dropdown>();
        sectionNameDd = GameObject.Find("SectionNameDd").GetComponent<Dropdown>();

        ScreenBtn = GameObject.Find("ScreenBtn").GetComponent<Button>();
        SettingBtn = GameObject.Find("SettingBtn").GetComponent<Button>();
        ScreenBtn.onClick.AddListener(onScreenBtnDown);
        SettingBtn.onClick.AddListener(onSettingBtnDown);
        LineSelectPanel = GameObject.Find("LineSelectPanel");
        ScreenPanel = GameObject.Find("ScreenPanel");


        refreshBtn = GameObject.Find("RefreshBtn").GetComponent<Button>();
        ScreenRefreshBtn = GameObject.Find("ScreenRefreshBtn").GetComponent<Button>();
        //generateBtn = GameObject.Find("GenerateBtn").GetComponent<Button>();
        refreshBtn.onClick.AddListener(onRefreshBtnDown);
        ScreenRefreshBtn.onClick.AddListener(onScreenRefreshBtnDown);
        //generateBtn.onClick.AddListener(onGenerateBtnDown);

        ColumnLineDd = GameObject.Find("ColumnLineDd").GetComponent<Dropdown>();
        StoryDd = GameObject.Find("StoryDd").GetComponent<Dropdown>();
        //ElementldDd = GameObject.Find("ElementldDd").GetComponent<Dropdown>();
        LoadCaseDd = GameObject.Find("LoadCaseDd").GetComponent<Dropdown>();

        ColumnLineDd.onValueChanged.AddListener(onScreenValueChange);
        StoryDd.onValueChanged.AddListener(onScreenValueChange);
        //ElementldDd.onValueChanged.AddListener(onScreenValueChange);
        LoadCaseDd.onValueChanged.AddListener(onScreenValueChange);




        ColumnTagNameDd.onValueChanged.AddListener(OnDGDdValueChange);
        StoryGroupNameDd.onValueChanged.AddListener(OnDGDdValueChange);
        sectionNameDd.onValueChanged.AddListener(OnSNDdValueChange);


        ScreenPanel.SetActive(false);
    }
    
    //IEnumerable FailLicence()
    //{
    //    yield return new WaitForSeconds(3s)
    //}


    //---------------------------文件加载-------------------------------
    //---------------------------文件加载-------------------------------
    #region
    public void StartLoadData() {
        //GetCurveData();
        //GetRDPMMData();

        DesignGroupList = GetDictKeys(DGDict);
        //SectionNameList = GetDictKeys(SNameDict);

        onRefreshBtnDown();

        text.text = GetDictKeys(SNameDict)[0] + "         ";
    }

    public bool isLoadRDPMMDataSuccess = false;
    public bool isLoadSDPMMDataSuccess = false;
    bool isLoadingRDP = false;
    public void LoadRDPMMData() {
        WrongMsg = "RDPMMResults : ";
        try
        {
            isLoadingRDP = true;
            GetRDPMMData();
            isLoadingRDP = false;
            isLoadRDPMMDataSuccess = true;
        }
        catch (System.Exception e)
        {
            Debug.Log(e.Message);
            isLoadingRDP = false;
            HasWrong = true;
            if (e.Message == "")
                return;
            WrongMsg = "RDPMMResults : The content of the csv file is incorrect.";
            throw;
        }
        
    }

    public void LoadSDPMMData()
    {
        WrongMsg = "SDPMMData : ";
        try
        {
            isLoadingRDP = true;
            GetCurveData();
            isLoadingRDP = false;
            isLoadSDPMMDataSuccess = true;
        }
        catch (System.Exception e)
        {
            isLoadingRDP = false;
            HasWrong = true;
            if (e.Message == "")
                return;
            WrongMsg = "SDPMMData : The content of the csv file is incorrect.";
            throw;
        }

    }


    public static bool HasWrong = false;
    public static string WrongMsg;

    bool hasOpen = false;
    bool hasDown = true;
    bool LoadSuccess = false;
    void UpdateLoadData() {
        if (LoadSuccess)
            return;

        if (isLoadingRDP && hasOpen == false) {
            MsgUI.Instance.ShowLoading(true);
            hasOpen = true;
            hasDown = false;
        }
        if (!isLoadingRDP && hasDown == false) {
            MsgUI.Instance.ShowLoading(false);
            hasOpen = false;
            hasDown = true;
        }

        if (HasWrong) {
            HasWrong = false;
            MsgUI.Instance.SetMsg(WrongMsg, true);
        }

        if (isLoadRDPMMDataSuccess && isLoadSDPMMDataSuccess) {
            LoadSuccess = true;
            StartLoadData();
            GameObject.Find("OpenFilePanel").SetActive(false);
            PlayerPrefs.SetString("RDPMMResultsPath", RDPMMPath);
            PlayerPrefs.SetString("SDPMMDataPath", SDPMMPath);
        }
    }
#endregion

    int index = 1;
	// Update is called once per frame
	void Update () {
        if (Input.GetKeyDown(KeyCode.Space)) {
            List<int> list = new List<int>();
            list.Add(index);
            index++;
            CreatCurve(list);
        }
        UpdateLoadData();
        hitPoint();
        CtrlCamera();
    }

    List<string> GetDictKeys(Dictionary<string,List<string>> dict) {
        List<string> list = new List<string>();
        foreach (string key in dict.Keys) {
            list.Add(key);
        }
        return list;
    }

    GameObject lastObj;
    Material lastMat;
    public void ResetMat() {
        if (lastObj != null)
            lastObj.GetComponent<MeshRenderer>().sharedMaterial = lastMat;
        lastObj = null;
    }

    Vector3 lastPos;
    Vector3 downPos;
    float hitTimer = 0;
    bool isHover = false;

    void hitPoint() {

        if (Vector3.SqrMagnitude(lastPos - Input.mousePosition) < 0.1)
        {
            hitTimer += Time.deltaTime;
        }
        else {
            hitTimer = 0;
            isHover = false;
        }

        if (hitTimer > 0.4f && isHover == false&& MsgUI.Instance.isClickMsg==false)
        {
            isHover = true;
            RaycastHit hit;
            if (Physics.Raycast(ca.ScreenPointToRay(Input.mousePosition), out hit, 10000))
            {
                if (lastObj != hit.transform.gameObject)
                {
                    if (lastObj != null)
                        lastObj.GetComponent<MeshRenderer>().sharedMaterial = lastMat;
                    lastObj = hit.transform.gameObject;
                    lastMat = hit.transform.GetComponent<MeshRenderer>().sharedMaterial;
                    hit.transform.GetComponent<MeshRenderer>().sharedMaterial = HitMat;
                }

                if (curPointsObj.Contains(hit.transform.gameObject))
                {
                    int index = curPointsObj.IndexOf(hit.transform.gameObject);
                    ShowPoint(curPoints[index]);
                }
            }
            else {
                MsgUI.Instance.HideMsg();
            }
        }



        if (Input.GetMouseButtonDown(0)) {
            downPos = Input.mousePosition;
        }

        if (Input.GetMouseButtonUp(0)&& Vector3.SqrMagnitude(downPos - Input.mousePosition) < 0.1) {
            RaycastHit hit;
            if (Physics.Raycast(ca.ScreenPointToRay(Input.mousePosition), out hit, 10000))
            {
                if (lastObj != hit.transform.gameObject)
                {
                    if (lastObj != null)
                        lastObj.GetComponent<MeshRenderer>().sharedMaterial = lastMat;
                    lastObj = hit.transform.gameObject;
                    lastMat = hit.transform.GetComponent<MeshRenderer>().sharedMaterial;
                    hit.transform.GetComponent<MeshRenderer>().sharedMaterial = HitMat;
                }

                if (curPointsObj.Contains(hit.transform.gameObject))
                {
                    int index = curPointsObj.IndexOf(hit.transform.gameObject);
                    ShowPoint(curPoints[index], true);
                }
            }
            downPos = Vector3.zero;
        }

        lastPos = Input.mousePosition;
    }

    Camera ca;
    float Speed = 5;
    Vector3 lastV3;
    void CtrlCamera()
    {
        if (!SectionPanel.IsEnterMask)
            return;

        float hor = Input.GetAxis("Horizontal");
        float ver = Input.GetAxis("Vertical");
        if (Input.GetMouseButton(0))
        {
                ca.transform.RotateAround(Vector3.zero, Vector3.forward, hor * Time.deltaTime * Speed);
                ca.transform.RotateAround(Vector3.zero, ca.transform.right, -ver * Time.deltaTime * Speed);

        }
        float scr = Input.GetAxis("Mouse ScrollWheel");
        if (scr != 0)
        {
            ca.orthographicSize -= scr * Time.deltaTime * 500f;
            float scale = ca.orthographicSize / 40;
            if (scale > 0.3f && scale < 1)
            {
                GameObject g = GameObject.Find("points");
                if (g != null)
                {
                    foreach (Transform t in g.transform)
                    {
                        t.localScale = Vector3.one * scale;
                    }
                }

            }
        }

        if (Input.GetMouseButtonDown(1)) {
            lastV3 = Input.mousePosition;
        }
        if (Input.GetMouseButton(1))
        {
            Vector3 dis = Input.mousePosition - lastV3;
            ca.transform.position -= (ca.transform.up * dis.y + ca.transform.right * dis.x) * 0.4f*ca.orthographicSize/ DrawSpeed;
            lastV3 = Input.mousePosition;
        }
    }


    void onSettingBtnDown() {
        if (LineSelectPanel.activeInHierarchy)
        {
            LineSelectPanel.SetActive(false);
        }
        else {
            LineSelectPanel.SetActive(true);
        }
    }

    void onScreenBtnDown() {
        if (ScreenPanel.activeInHierarchy)
        {
            ScreenPanel.SetActive(false);
        }
        else
        {
            ScreenPanel.SetActive(true);
        }
    }

    #region -------------------------画线---------------------------------
    List<string> DesignGroupList;
    List<RDPMMNode> curPoints;
    List<GameObject> curPointsObj;

    void OnDGDdValueChange(int value) {
        if (ColumnTagNameDd.value == 0 || StoryGroupNameDd.value == 0)
            return;

        string columnTagName = ColumnTagNameDd.options[ColumnTagNameDd.value].text;
        string storyGroupName = StoryGroupNameDd.options[StoryGroupNameDd.value].text;
        string DGName = columnTagName + "," + storyGroupName;

        if (sectionNameDd.value == 0)
        {
            sectionNameDd.ClearOptions();
            List<string> list = new List<string>();
            list.Add("SectionName");
            list.AddRange(DGDict[DGName]);
            sectionNameDd.AddOptions(list);
        }
        else {
            onGenerateBtnDown();
        }
    }

    void OnSNDdValueChange(int value)
    {
        if (sectionNameDd.value == 0)
            return;
        string columnTagName = ColumnTagNameDd.options[ColumnTagNameDd.value].text;
        string storyGroupName = StoryGroupNameDd.options[StoryGroupNameDd.value].text;
        string DGName = columnTagName + "," + storyGroupName;
        string sectionName = sectionNameDd.options[sectionNameDd.value].text;
        string key = DGName + sectionName;
        if (ColumnTagNameDd.value == 0 || StoryGroupNameDd.value == 0|| !DG2SNamePmmIdDict.ContainsKey(key))
        {
            List<string> columnList = new List<string>();
            List<string> storyList = new List<string>();
            columnList.Add("ColumnTagName");
            storyList.Add("StoryGroupName");
            foreach (string dg in SNameDict[sectionName])
            {
                string[] s = dg.Split(',');
                if(!columnList.Contains(s[0]))
                    columnList.Add(s[0]);
                if (!storyList.Contains(s[1]))
                    storyList.Add(s[1]);
            }

            ColumnTagNameDd.ClearOptions();
            ColumnTagNameDd.AddOptions(columnList);
            ColumnTagNameDd.value = 0;

            StoryGroupNameDd.ClearOptions();
            StoryGroupNameDd.AddOptions(storyList);
            StoryGroupNameDd.value = 0;
        }
        else {
            
            if (!DG2SNamePmmIdDict.ContainsKey(key))
            {
                return;
            }

            onGenerateBtnDown();
        }
    }

    void onRefreshBtnDown() {
        ColumnTagNameDd.ClearOptions();
        ColumnTagNameDd.AddOptions(ColumnTagList);
        ColumnTagNameDd.value = 0;

        StoryGroupNameDd.ClearOptions();
        StoryGroupNameDd.AddOptions(StoryGroupList);
        StoryGroupNameDd.value = 0;

        sectionNameDd.ClearOptions();
        sectionNameDd.AddOptions(SectionNameList);
        sectionNameDd.value = 0;
    }
    void onScreenRefreshBtnDown()
    {
        ColumnLineDd.value = 0;
        StoryDd.value = 0;
        LoadCaseDd.value = 0;
        onScreenValueChange(0);
    }


    void onGenerateBtnDown() {
        string columnTagName = ColumnTagNameDd.options[ColumnTagNameDd.value].text;
        string storyGroupName = StoryGroupNameDd.options[StoryGroupNameDd.value].text;
        string DGName = columnTagName + "," + storyGroupName;
        string key = DGName + sectionNameDd.options[sectionNameDd.value].text;
        if (!DG2SNamePmmIdDict.ContainsKey(key)) {
            return;
        }
        CreatCurve(DG2SNamePmmIdDict[key]);
        curPoints = DG2SNameDict[key];
        creatscreen();
    }

    void creatscreen() {
        Destroy(GameObject.Find("points"));
        Transform points = new GameObject("points").transform;
        List<string> ColumnLineList = new List<string>();
        List<string> StoryList = new List<string>();
        //List<string> ElementldList = new List<string>();
        List<string> LoadCaseList = new List<string>();
        curPointsObj = new List<GameObject>();
        ColumnLineList.Add("All");
        StoryList.Add("All");
        //ElementldList.Add("All");
        LoadCaseList.Add("All");
        foreach (var point in curPoints)
        {
            if (!ColumnLineList.Contains(point.ColumnLine))
            {
                ColumnLineList.Add(point.ColumnLine);
            }
            if (!StoryList.Contains(point.Story))
            {
                StoryList.Add(point.Story);
            }
            //if (!ElementldList.Contains(point.ElementId))
            //{
            //    ElementldList.Add(point.ElementId);
            //}
            if (!LoadCaseList.Contains(point.LoadCase))
            {
                LoadCaseList.Add(point.LoadCase);
            }

            GameObject g = Instantiate(Node);
            g.transform.position = new Vector3(point.X, point.Y, point.Z / 2.5f) / 100f;
            if (point.Remarks.Contains("Passed"))
            {
                g.GetComponent<MeshRenderer>().sharedMaterial = Passed;
            }
            else {
                g.GetComponent<MeshRenderer>().sharedMaterial = Failed;
            }
            g.transform.SetParent(points);
            curPointsObj.Add(g);

        }
        ColumnLineDd.ClearOptions();
        ColumnLineDd.AddOptions(ColumnLineList);
        ColumnLineDd.value = 0;

        StoryDd.ClearOptions();
        StoryDd.AddOptions(StoryList);
        StoryDd.value = 0;

        //ElementldDd.ClearOptions();
        //ElementldDd.AddOptions(ElementldList);
        //ElementldDd.value = 0;

        LoadCaseDd.ClearOptions();
        LoadCaseDd.AddOptions(LoadCaseList);
        LoadCaseDd.value = 0;
        points.localScale = new Vector3(1, 1.001f, 1);
    }

    void onScreenValueChange(int value) {
        lastObj = null;

        string ColumnLine = ColumnLineDd.options[ColumnLineDd.value].text;
        string Story = StoryDd.options[StoryDd.value].text;
        //string ElementId = ElementldDd.options[ElementldDd.value].text;
        string LoadCase = LoadCaseDd.options[LoadCaseDd.value].text;
        for (int i=0; i< curPoints.Count;i++) {
            var point = curPoints[i];
            if (ColumnLine != "All" && point.ColumnLine != ColumnLine)
            {
                if (point.Remarks.Contains("Passed"))
                    curPointsObj[i].GetComponent<MeshRenderer>().sharedMaterial = Hide_Passed;
                else
                    curPointsObj[i].GetComponent<MeshRenderer>().sharedMaterial = Hide_Failed;
                curPointsObj[i].GetComponent<SphereCollider>().enabled = false;
            }
            else if (LoadCase != "All" && point.LoadCase != LoadCase)
            {
                if (point.Remarks.Contains("Passed"))
                    curPointsObj[i].GetComponent<MeshRenderer>().sharedMaterial = Hide_Passed;
                else
                    curPointsObj[i].GetComponent<MeshRenderer>().sharedMaterial = Hide_Failed;
                curPointsObj[i].GetComponent<SphereCollider>().enabled = false;
            }
            else if (Story != "All" && point.Story != Story)
            {
                if (point.Remarks.Contains("Passed"))
                    curPointsObj[i].GetComponent<MeshRenderer>().sharedMaterial = Hide_Passed;
                else
                    curPointsObj[i].GetComponent<MeshRenderer>().sharedMaterial = Hide_Failed;
                curPointsObj[i].GetComponent<SphereCollider>().enabled = false;
            }
            //else if (ElementId != "All" && point.ElementId != ElementId)
            //{
            //    curPointsObj[i].GetComponent<MeshRenderer>().sharedMaterial = Hide;
            //    curPointsObj[i].GetComponent<SphereCollider>().enabled = false;
            //}
            else {
                if (point.Remarks.Contains("Passed"))
                {
                    curPointsObj[i].GetComponent<MeshRenderer>().sharedMaterial = Passed;
                    curPointsObj[i].GetComponent<SphereCollider>().enabled = true;
                }
                else
                {
                    curPointsObj[i].GetComponent<MeshRenderer>().sharedMaterial = Failed;
                    curPointsObj[i].GetComponent<SphereCollider>().enabled = true;
                }
            }
        }
    }

    #endregion

    Dictionary<int, List<CurvePoint>> pmmIDDict;


    #region ---------------------读取点数据---------------------------
    Dictionary<string, List<string>> DGDict;
    Dictionary<string, List<string>> SNameDict;

    Dictionary<string, List<RDPMMNode>> DG2SNameDict;
    Dictionary<string, List<int>> DG2SNamePmmIdDict;
    //Dictionary<string, List<RDPMMNode>> SName2DGDict;

    

    List<string> ColumnTagList;
    List<string> StoryGroupList;
    List<string> SectionNameList;
    void GetRDPMMData() {
        string[] RDPMMNodeNames = {"ColumnTagName", "ColumnLine", "Story","ElementId" ,"SectionId", "SectionName",
        "PMM ID", "LoadCase","InitialP","InitialMx","InitialMy","PMM DCR","Remarks","StoryGroupName",
        "ShiftedP","ShiftedMx","ShiftedMy","CapacityP","CapacityMx","CapacityMy"  };
        int[] RDPMMNodeIndexs = new int[20];
        DGDict = new Dictionary<string, List<string>>();
        SNameDict = new Dictionary<string, List<string>>();
        DG2SNameDict = new Dictionary<string, List<RDPMMNode>>();
        DG2SNamePmmIdDict = new Dictionary<string, List<int>>();
        ColumnTagList = new List<string>();
        ColumnTagList.Add("ColumnTagName");
        StoryGroupList = new List<string>();
        StoryGroupList.Add("StoryGroupName");
        SectionNameList = new List<string>();
        SectionNameList.Add("SectionName");
        //SName2DGDict = new Dictionary<string, List<RDPMMNode>>();
        CSV.GetInstance().LoadFile(RDPMMPath);
        List<string[]> data = CSV.GetInstance().m_ArrayData;
        Debug.Log(data.Count);

        for (int i = 0; i < RDPMMNodeNames.Length; i++) {
            int index = CSV.GetInstance().GetIndexByName(RDPMMNodeNames[i]);
            if (index == -1) {
                WrongMsg = "RDPMMResults : Can't find "+ RDPMMNodeNames[i];
                throw new System.Exception("");
            }
            RDPMMNodeIndexs[i] = index;
        }

        string dg2sn = "";
        //string sn2dg = "";
        for (int i = 1; i < data.Count; i++)
        {
            RDPMMNode cp = new RDPMMNode();
            cp.ColumnTagName = data[i][RDPMMNodeIndexs[0]];
            cp.ColumnLine = data[i][RDPMMNodeIndexs[1]];
            cp.Story = data[i][RDPMMNodeIndexs[2]];
            //cp.ElementId = data[i][RDPMMNodeIndexs[3]];
            cp.SectionId = data[i][RDPMMNodeIndexs[4]];
            cp.SectionName = data[i][RDPMMNodeIndexs[5]];
            cp.PmmID = int.Parse(data[i][RDPMMNodeIndexs[6]]);
            cp.LoadCase = data[i][RDPMMNodeIndexs[7]];
            cp.Z = float.Parse(data[i][RDPMMNodeIndexs[8]]);
            cp.X = float.Parse(data[i][RDPMMNodeIndexs[9]]);
            cp.Y = float.Parse(data[i][RDPMMNodeIndexs[10]]);
            cp.DCR = float.Parse(data[i][RDPMMNodeIndexs[11]]);
            cp.Remarks = data[i][RDPMMNodeIndexs[12]];
            cp.StoryGroupName = data[i][RDPMMNodeIndexs[13]];
            cp.ShiftedP = float.Parse(data[i][RDPMMNodeIndexs[14]]);
            cp.ShiftedMx = float.Parse(data[i][RDPMMNodeIndexs[15]]);
            cp.ShiftedMy = float.Parse(data[i][RDPMMNodeIndexs[16]]);
            cp.CapacityP = float.Parse(data[i][RDPMMNodeIndexs[17]]);
            cp.CapacityMx = float.Parse(data[i][RDPMMNodeIndexs[18]]);
            cp.CapacityMy = float.Parse(data[i][RDPMMNodeIndexs[19]]);
            cp.DesignGroupName = cp.ColumnTagName +","+ cp.StoryGroupName;

            if (!ColumnTagList.Contains(cp.ColumnTagName))
                ColumnTagList.Add(cp.ColumnTagName);

            if (!StoryGroupList.Contains(cp.StoryGroupName))
                StoryGroupList.Add(cp.StoryGroupName);

            if (!DGDict.ContainsKey(cp.DesignGroupName))
            {
                DGDict.Add(cp.DesignGroupName, new List<string>());
            }
            if (!DGDict[cp.DesignGroupName].Contains(cp.SectionName))
            {
                DGDict[cp.DesignGroupName].Add(cp.SectionName);
                dg2sn = cp.DesignGroupName + cp.SectionName;
                DG2SNameDict.Add(dg2sn, new List<RDPMMNode>());
                DG2SNamePmmIdDict.Add(dg2sn, new List<int>());
            }
            DG2SNameDict[dg2sn].Add(cp);

            if (!DG2SNamePmmIdDict[dg2sn].Contains(cp.PmmID))
            {
                DG2SNamePmmIdDict[dg2sn].Add(cp.PmmID);
            }

            if (!SNameDict.ContainsKey(cp.SectionName))
            {
                SectionNameList.Add(cp.SectionName);
                SNameDict.Add(cp.SectionName, new List<string>());
            }
            if (!SNameDict[cp.SectionName].Contains(cp.DesignGroupName))
            {
                SNameDict[cp.SectionName].Add(cp.DesignGroupName);
                //sn2dg = cp.DesignGroupName + cp.SectionName;
                //SName2DGDict.Add(sn2dg, new List<RDPMMNode>());
            }
            //SName2DGDict[sn2dg].Add(cp);

        }
        CSV.GetInstance().m_ArrayData.Clear();
    }

    #endregion


    #region     ---------------------创建洋葱---------------------------

    void GetCurveData() {
        pmmIDDict = new Dictionary<int, List<CurvePoint>>();
        CSV.GetInstance().LoadFile(SDPMMPath);
        List<string[]> data = CSV.GetInstance().m_ArrayData;
        Debug.Log(data.Count);

        for (int i = 0; i < CurvePointNames.Length; i++)
        {
            int index = CSV.GetInstance().GetIndexByName(CurvePointNames[i]);
            if (index == -1) {
                WrongMsg = "RDPMMCurve : Can't find " + CurvePointNames[i];
                throw new System.Exception("");
            }
            CurvePointIndexs[i] = index;
        }

        for (int i = 1; i < data.Count; i++)
        {
            CurvePoint cp = new CurvePoint();
            cp.PmmID = int.Parse(data[i][CurvePointIndexs[0]]);
            cp.Z = float.Parse(data[i][CurvePointIndexs[1]]);
            cp.X = float.Parse(data[i][CurvePointIndexs[2]]);
            cp.Y = float.Parse(data[i][CurvePointIndexs[3]]);
            cp.Queue = int.Parse(data[i][CurvePointIndexs[4]]);

            if (!pmmIDDict.ContainsKey(cp.PmmID))
            {
                pmmIDDict.Add(cp.PmmID, new List<CurvePoint>());
            }
            pmmIDDict[cp.PmmID].Add(cp);
        }
    }

    void CreatCurve(List<int> pmmidList) {
        var isLog= LogFeature("New PMM");
        Debug.Log("New PMM:"+isLog);

        Destroy(GameObject.Find("lines"));
        Transform lines = new GameObject("lines").transform;
        List<LineRenderer> lineList = new List<LineRenderer>();
        foreach (int pmmid in pmmidList) {
            Dictionary<int, List<Vector3>> queueV3 = new Dictionary<int, List<Vector3>>();
            if (pmmIDDict.ContainsKey(pmmid)) {

                foreach (var node in pmmIDDict[pmmid]) {
                    if (!queueV3.ContainsKey(node.Queue)) {
                        queueV3.Add(node.Queue, new List<Vector3>());
                    }
                    queueV3[node.Queue].Add(new Vector3(node.X, node.Y, node.Z / 2.5f) / 100f);

                }

                foreach(var q in queueV3.Values){
                    Debug.Log(q.Count);
                    LineRenderer line = Instantiate(Line).GetComponent<LineRenderer>();
                    line.positionCount = q.Count;
                    line.SetPositions(q.ToArray());
                    line.transform.SetParent(lines);
                    lineList.Add(line);
                }

            }
        }

        for (int i = 0; i < lineList[0].positionCount; i++)
        {
            if (i % 2 == 0)
                continue;
            Vector3[] notes = new Vector3[lineList.Count];
            for (int j = 0; j < lineList.Count; j++)
            {
                notes[j] = lineList[j].GetPosition(i);
            }
            LineRenderer line = Instantiate(Line).GetComponent<LineRenderer>();
            line.loop = true;
            line.positionCount = lineList.Count;
            line.SetPositions(notes);
            line.transform.SetParent(lines);
        }

    }

    #endregion---------------------创建洋葱---------------------------
    //string[] RDPMMNodeNames = {"ColumnTagName", "ColumnLine", "Story","ElementId" ,"SectionId", "SectionName",
        //"PMM ID", "LoadCase","InitialP","InitialMx","InitialMy","PMM DCR","Remarks","StoryGroupName",
        //"ShiftedP","ShiftedMx","ShiftedMy","CapacityP","CapacityMx","CapacityMy"  };
    //ColumnLineName, Story, LoadCase, ForceP, ForceMx, ForceMy, DCR
    void ShowPoint(RDPMMNode point,bool isClickMsg=false) {
        //Math.Round((decimal)1f, 2, MidpointRounding.AwayFromZero)
        string msg = "ColumnLine :  " + point.ColumnLine + "*";
        msg += "Story :  " + point.Story + "*";
        msg += "LoadCase :  " + point.LoadCase + "*";
        msg += "Axial Demand :  " + Math.Round((decimal)point.ShiftedP, 2, MidpointRounding.AwayFromZero) + " kN*";
        msg += "Moment X Demand :  " + Math.Round((decimal)point.ShiftedMx, 2, MidpointRounding.AwayFromZero) + " kN-m*";
        msg += "Moment Y Demand :  " + Math.Round((decimal)point.ShiftedMy, 2, MidpointRounding.AwayFromZero) + " kN-m*";
        msg += "Axial Capacity :  " + Math.Round((decimal)point.CapacityP, 2, MidpointRounding.AwayFromZero) + " kN*";
        msg += "Moment X Capacity :  " + Math.Round((decimal)point.CapacityMx, 2, MidpointRounding.AwayFromZero) + " kN-m*";
        msg += "Moment Y Capacity :  " + Math.Round((decimal)point.CapacityMy, 2, MidpointRounding.AwayFromZero) + " kN-m*";
        msg += "PMM DCR :  " + Math.Round((decimal)point.DCR, 2, MidpointRounding.AwayFromZero);
        msg = msg.Replace('*', '\n');
        MsgUI.Instance.SetMsg(msg, isClickMsg);
    }

    public string[] CurvePointNames = { "PMM ID", "Axial", "Moment X", "Moment Y", "Neutral Angle/Curve Point" };
    public int[] CurvePointIndexs = new int[5];

}

public class CurvePoint {
    public int PmmID;
    //public string PmmType;
    //public string SectionId;
    public float Z;
    public float X;
    public float Y;
    public int Queue;
}

public class RDPMMNode {
    public string ColumnTagName;
    public string ColumnLine;
    public string Story;
    public string ElementId;
    public string SectionId;
    public string SectionName;
    public int PmmID;
    public string LoadCase;
    public float Z;
    public float X;
    public float Y;
    public float DCR;
    public string Remarks;
    public string StoryGroupName;
    public float ShiftedP;
    public float ShiftedMx;
    public float ShiftedMy;
    public float CapacityP;
    public float CapacityMx;
    public float CapacityMy;
    public string DesignGroupName;
}



//using System;
//using System.Collections.Generic;
//using System.Linq;
//using System.Text;
//using System.Threading.Tasks;
//using ArmadilloCsharp;
//namespace ArmadilloCSharpTestConsoleApp
//{
//    class Program
//    {
//        static void Main(string[] args)
//        {
//            var appName = "ArmadilloCSharpTestConsoleApp";
//            var appVersion = "1.0";
//            var isLicensed = Licence.CheckLicence(appName, appVersion, true);
//            var trackInfo = new TrackingInfo("TestFeat");
//            var success = trackInfo.StartFeature();
//            success = trackInfo.LogFeature();
//            success = trackInfo.EndFeature();
//            Console.ReadLine();
//        }
//    }
//}