﻿using UnityEngine;
using System.IO;
using System.Collections.Generic;

public class CSV
{
    private static readonly object Lock = new object();
    static CSV csv;
    public List<string[]> m_ArrayData;
    public static CSV GetInstance()
    {
        if (csv == null)
        {
            csv = new CSV();
        }
        return csv;
    }
    private CSV() { m_ArrayData = new List<string[]>(); }
    public string GetString(int row, int col)
    {
        return m_ArrayData[row][col];
    }
    public int GetInt(int row, int col)
    {
        return int.Parse(m_ArrayData[row][col]);
    }
    public void LoadFile(string path)
    {
        m_ArrayData.Clear();
        StreamReader sr = null;
        try
        {
            sr = File.OpenText(path);
            Debug.Log("file finded!");
        }
        catch
        {
            Debug.Log(path);
            Debug.Log("file don't finded!");
            PPMCtrl.WrongMsg += "File don't finded!";
            throw new System.Exception("");
        }
        string line;
        while ((line = sr.ReadLine()) != null)
        {
            m_ArrayData.Add(line.Split(','));
        }
        sr.Close();
        sr.Dispose();
    }

    public int GetIndexByName(string name) {
        lock (Lock)
        {
            try
            {
                for (int i = 0; i < m_ArrayData[0].Length; i++)
                {
                    if (m_ArrayData[0][i] == name)
                        return i;
                }
                return -1;
            }
            catch (System.Exception e)
            {
                Debug.Log("00000  " + e.Message);
                throw;
            }
            
        }
        
    }
}