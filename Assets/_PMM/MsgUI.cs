﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class MsgUI : MonoBehaviour ,IPointerDownHandler{
    public void OnPointerDown(PointerEventData eventData)
    {
        gameObject.SetActive(false);
        ctrl.ResetMat();
        isClickMsg = false;
    }
    Text txt;
    public static MsgUI Instance;
    public GameObject Loading;
    PPMCtrl ctrl;
    void Awake() {
        Instance = this;
    }

    void Start () {
        txt = gameObject.GetComponentInChildren<Text>();
        ctrl = GameObject.FindObjectOfType<PPMCtrl>();
        Loading.SetActive(false);
        gameObject.SetActive(false);
    }

    public bool isClickMsg = false;

    public void SetMsg(string msg,bool isClick=false) {
        if (isClickMsg == true && isClick == false)
            return;
        txt.text = msg;
        gameObject.SetActive(true);
        isClickMsg = isClick;
    }

    public void HideMsg() {
        if (!isClickMsg) {
            gameObject.SetActive(false);
            ctrl.ResetMat();
        }
    }

    public void ShowLoading(bool value) {
        Loading.SetActive(value);
    }
}
