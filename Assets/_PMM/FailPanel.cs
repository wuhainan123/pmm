﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FailPanel : MonoBehaviour {

	// Use this for initialization
	void Start () {
        Invoke("ShutDown", 4f);
	}

    void ShutDown() {
        Application.Quit();
    }
}
