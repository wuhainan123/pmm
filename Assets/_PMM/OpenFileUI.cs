﻿using UnityEngine;
using UnityEngine.UI;
using System.Threading;
using System.Collections;
public class OpenFileUI : MonoBehaviour {
    public Text RDPMMResultsPathTxt;
    public Text SDPMMDataPathTxt;
    
    public Button RDPMMResultsBtn;
    public Button SDPMMDataBtn;
    public Button StartBtn;

    OpenFile opFile;

    PPMCtrl ppmCtrl;
    // Use this for initialization
    void Start () {
        //Application.dataPath + "/CSVs/RDPMMCurve.csv";
        string rpath = PlayerPrefs.GetString("RDPMMResultsPath");
        if (rpath == "" || rpath == null)
        {
            RDPMMResultsPathTxt.text = "RDPMMResults : " + Application.dataPath + "/CSVs/RDPMMResults.csv";
            rpath= Application.dataPath + "/CSVs/RDPMMResults.csv";
        }
        else
            RDPMMResultsPathTxt.text = "RDPMMResults : " + rpath;

        string spath = PlayerPrefs.GetString("SDPMMDataPath");
        if (spath == "" || spath == null)
        {
            SDPMMDataPathTxt.text = "RDPMMCurve : " + Application.dataPath + "/CSVs/RDPMMCurve.csv";
            spath = Application.dataPath + "/CSVs/RDPMMCurve.csv";
        }
        else
            SDPMMDataPathTxt.text = "RDPMMCurve : " + spath;
        opFile = GameObject.FindObjectOfType<OpenFile>();

        RDPMMResultsBtn.onClick.AddListener(OnRDPMMResultsBtnDown);
        SDPMMDataBtn.onClick.AddListener(OnSDPMMDataBtnDown);
        StartBtn.onClick.AddListener(OnStartBtnDown);


        ppmCtrl = GameObject.FindObjectOfType<PPMCtrl>();
        ppmCtrl.RDPMMPath = rpath;
        ppmCtrl.SDPMMPath = spath;
    }
	
	// Update is called once per frame
	void Update () {
		
	}

    void OnRDPMMResultsBtnDown() {
        string path = opFile.OpenFileWin();
        if (path != null) {
            ppmCtrl.RDPMMPath = path;
            RDPMMResultsPathTxt.text = "RDPMMResults : " + path;
            Thread thread = new Thread(ppmCtrl.LoadRDPMMData);
            thread.Start();
        }
    }

    void OnSDPMMDataBtnDown()
    {
        string path = opFile.OpenFileWin();
        if (path != null)
        {
            ppmCtrl.SDPMMPath = path;
            SDPMMDataPathTxt.text = "RDPMMCurve : " + path;
            Thread thread = new Thread(ppmCtrl.LoadSDPMMData);
            thread.Start();
        }
    }

    void OnStartBtnDown() {
        StopCoroutine("LoadAll");
        StartCoroutine("LoadAll");

    }

    IEnumerator LoadAll() {
        ppmCtrl.isLoadSDPMMDataSuccess = false;
        ppmCtrl.isLoadRDPMMDataSuccess = false;
        Thread thread = new Thread(ppmCtrl.LoadSDPMMData);
        thread.Start();
        while (!ppmCtrl.isLoadSDPMMDataSuccess)
        {
            yield return null;
        }

        Thread thread1 = new Thread(ppmCtrl.LoadRDPMMData);
        thread1.Start();
    }
    
}
