﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class SectionPanel : MonoBehaviour ,IPointerExitHandler,IPointerEnterHandler{

	public void OnPointerEnter (PointerEventData eventData)
	{
		IsEnterMask = true;
	}



	public static bool IsEnterMask = false;

	public void OnPointerExit (PointerEventData eventData)
	{
		IsEnterMask = false;
	}

}
