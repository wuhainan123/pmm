﻿using UnityEngine;
using System.Collections;
using System.Runtime.InteropServices;
using System.Reflection;
using UnityEngine.Video;
using UnityEngine.UI;
public class OpenFile : MonoBehaviour
{
    public string OpenFileWin()
    {

        OpenFileName ofn = new OpenFileName();

        ofn.structSize = Marshal.SizeOf(ofn);
        string type = "csv";
        //ofn.filter = "All Files\0*.*\0\0";
        ofn.filter = "文件(*." + type + ")\0*." + type + "";

        ofn.file = new string(new char[256]);

        ofn.maxFile = ofn.file.Length;

        ofn.fileTitle = new string(new char[64]);

        ofn.maxFileTitle = ofn.fileTitle.Length;
        string path = Application.dataPath+"/CSVs";
        path = path.Replace('/', '\\');
        //默认路径  
        ofn.initialDir = path;
        //ofn.initialDir = "D:\\MyProject\\UnityOpenCV\\Assets\\StreamingAssets";  
        ofn.title = "选择文件";

        //ofn.defExt = "JPG";//显示文件的类型  
        //注意 一下项目不一定要全选 但是0x00000008项不要缺少  
        ofn.flags = 0x00080000 | 0x00001000 | 0x00000800 | 0x00000200 | 0x00000008;//OFN_EXPLORER|OFN_FILEMUSTEXIST|OFN_PATHMUSTEXIST| OFN_ALLOWMULTISELECT|OFN_NOCHANGEDIR  

        if (WindowDll.GetOpenFileName(ofn))
        {
            //Debug.Log(ofn.defExt);
            //Debug.Log("Selected file with full path: {0}" + ofn.file);
            return ofn.file;
        }
        return null;
    }

}  