﻿using UnityEngine;

public class DrawRectTest : MonoBehaviour {

	bool drawing = false;
	private Material rectMat;
	private Color rectColor = Color.green;
	private Vector2 startPosition = Vector2.zero;
	private Vector2 endPosition = Vector2.zero;
	BuildCube bc;

	void Start()
	{
		rectMat = new Material( Shader.Find( "Lines/Colored_Blended" ) );
		bc = GameObject.FindObjectOfType<BuildCube> ();

	}

	void Update()
	{
		if (Input.GetMouseButtonDown(1))
		{
			drawing = true;
			startPosition = Input.mousePosition;    // 设置开始位置
			bc.ca=GetComponent<Camera>();
			bc.BeginDraw();
		}

		else if ( Input.GetMouseButtonUp( 1 ) )
		{
			drawing = false;
			if (Vector3.SqrMagnitude (startPosition - endPosition) < 0.1f) {
				bc.OnClick (startPosition);
			} else {
				bc.EndDraw(startPosition, endPosition );
			}
		}
	}

	float timer=0;

	void OnPostRender()     // 该方法为系统函数，需将此脚本挂载到Camera下才能执行。
	{
		if ( drawing )
		{
			timer += Time.deltaTime;
			GL.PushMatrix();

			if ( !rectMat )
				return;

			endPosition = Input.mousePosition;      // 设置结束位置

			if (timer > 0.15f) {
				bc.Drawing( startPosition, endPosition );
				timer = 0;
			}
			rectMat.SetPass( 0 );
			GL.LoadPixelMatrix();
			GL.Begin( GL.QUADS );
			GL.Color( new Color( rectColor.r, rectColor.g, rectColor.b, 0.1f ) );
			GL.Vertex3( startPosition.x, startPosition.y, 0 );
			GL.Vertex3( endPosition.x, startPosition.y, 0 );
			GL.Vertex3( endPosition.x, endPosition.y, 0 );
			GL.Vertex3( startPosition.x, endPosition.y, 0 );
			GL.End();
			GL.Begin( GL.LINES );
			GL.Color( rectColor );
			GL.Vertex3( startPosition.x, startPosition.y, 0 );
			GL.Vertex3( endPosition.x, startPosition.y, 0 );
			GL.Vertex3( endPosition.x, startPosition.y, 0 );
			GL.Vertex3( endPosition.x, endPosition.y, 0 );
			GL.Vertex3( endPosition.x, endPosition.y, 0 );
			GL.Vertex3( startPosition.x, endPosition.y, 0 );
			GL.Vertex3( startPosition.x, endPosition.y, 0 );
			GL.Vertex3( startPosition.x, startPosition.y, 0 );
			GL.End();
			GL.PopMatrix();
		}
	}

}
