﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using LitJson;
using UnityEngine.UI;
public class BuildCube : MonoBehaviour {

	public GameObject NodeObj;
	public GameObject Edge;
	Dictionary<string,List<Item>> columnDict;
	Dictionary<string,List<Item>> storyDict;

	Dictionary<string,Item> guidItemDict;
	Dictionary<string,GameObject> guidObjDict;

	Dictionary<string,int> columnSelectCountDict;
	Dictionary<string,int> storySelectCountDict;
	Dictionary<string,MeshRenderer> guidMeshDict;

	public Dictionary<string,List<string>> storyGroup;
	public Dictionary<string,List<string>> columnGroup;


	public int TestNum;
	public string Guid;
	public string column;

	Button CheckBtn;

	public Material selectMat;
	public Material checkMat;
	public Material startMat;


//	public Dictionary

	void Start(){
		columnDict = new Dictionary<string, List<Item>> ();
		storyDict = new Dictionary<string, List<Item>> ();
		guidItemDict = new Dictionary<string, Item> ();
		guidObjDict = new Dictionary<string, GameObject> ();
		columnSelectCountDict = new Dictionary<string, int> ();
		storySelectCountDict = new Dictionary<string, int> ();
		guidMeshDict = new Dictionary<string, MeshRenderer> ();
		storyGroup = new Dictionary<string, List<string>> ();
		columnGroup = new Dictionary<string, List<string>> ();
		ca = GameObject.Find ("Main Camera").GetComponent<Camera> ();

		CheckBtn = GameObject.Find ("CheckBtn").GetComponent<Button> ();
		CheckBtn.onClick.AddListener (onCheckGroupClick);
	}

	string jsontest;
	public string SGroup;
	public string CGroup;
	void Update () {
		if (Input.GetKeyDown (KeyCode.N)) {
			addStoryGroup (Random.Range (0, 21544).ToString ());
			addColumnGroup (Random.Range (0, 21544).ToString ());
		}

        //if (Input.GetKeyDown(KeyCode.D)) {
        //    DesignGroup dg = new DesignGroup();
        //    dg.ColumnGroupID = CGroup;
        //    dg.StoryGroupID = SGroup;
        //    shineSingleGroup(JsonMapper.ToJson(dg));
        //}

		if (Input.GetKeyDown (KeyCode.C)) {
			changeStage ("ColumnGroup");
		}
		if (Input.GetKeyDown (KeyCode.S)) {
			changeStage ("StoryGroup");
		}
		if (Input.GetKeyDown (KeyCode.A)) {
			resetMeshColor ();
			showAllDesignGroup ();
		}

		if (Input.GetKeyDown (KeyCode.T)) {
			shineSingleGroup (CGroup);
		}
		CtrlCamera ();
		shineMatUpdate ();
	}

	float MaxX;
	float MaxY;
	float MaxZ;
	float MinX;
	float MinY;
	float MinZ;
	Color StartColor = new Color (0.7f, 0.7f, 0.7f);
	public void StartBuild (Item[] items) {
		storyMeshRendererList = new List<MeshRenderer> ();
		columnMeshRendererList = new List<MeshRenderer> ();
		meshRendererListAll = new List<MeshRenderer> ();
		meshInScreen = new List<Vector3> ();

		Transform nodeParent = new GameObject ("Nodes").transform;
		nodeParent.position = Vector3.zero;
		nodeParent.rotation = Quaternion.identity;

		foreach (Item node in items) {
			GetEdge (node);

			GameObject g= Instantiate (NodeObj);
			g.transform.position = node.startNode.GetVec3 ();
			g.transform.localScale = new Vector3 (1f,1f, node.GetHeight ());
			g.name = node.guid;
			g.transform.SetParent (nodeParent);

			MeshRenderer meshtemp = g.GetComponentInChildren<MeshRenderer> ();
			storyMeshRendererList.Add (meshtemp);
			columnMeshRendererList.Add (meshtemp);
			meshRendererListAll.Add (meshtemp);
			meshInScreen.Add (Vector3.zero);
			guidMeshDict.Add (node.guid, meshtemp);

			meshtemp.material=startMat;

			if (columnDict.ContainsKey (node.columnLineID)) {
				columnDict [node.columnLineID].Add (node);
//				meshtemp.material.color = guidObjDict[columnDict [node.columnName][0].guid].GetComponentInChildren<MeshRenderer> ().material.color;
			} else {
				List<Item> temp = new List<Item> ();
				temp.Add (node);
				columnDict.Add (node.columnLineID, temp);
//				meshtemp.material.color = new Color (Random.Range (0f, 1f), Random.Range (0f, 1f), Random.Range (0f, 1f));

				columnSelectCountDict.Add (node.columnLineID, 0);
			}

			if (storyDict.ContainsKey (node.storyID)) {
				storyDict [node.storyID].Add (node);
			} else {
				List<Item> temp = new List<Item> ();
				temp.Add (node);
				storyDict.Add (node.storyID, temp);
				storySelectCountDict.Add (node.storyID, 0);
			}
			guidItemDict.Add (node.guid, node);
			guidObjDict.Add (node.guid, g);

//			g.GetComponentInChildren<MeshRenderer> ().material.color = new Color (Random.Range (0f, 1f), Random.Range (0f, 1f), Random.Range (0f, 1f));
		}

		CreateEdge ();



		GameObject.FindObjectOfType<BuildAxis> ().SetAxis (edge.transform,MinZ);

		float maxOff = (MaxX - MinX) > (MaxY - MinY) ? (MaxX - MinX) : (MaxY - MinY);
		maxOff = maxOff > (MaxZ - MinZ) ? maxOff : (MaxZ - MinZ);
		edge.transform.localScale = Vector3.one * maxOff;

		Transform t = edge.transform.Find ("F");
		ca.transform.position = t.transform.position;
		ca.transform.rotation = t.rotation;
		nodeParent.localScale = new Vector3 (1, 1, 1.001f);

		OnDataGetComplete ();
	}

	GameObject edge;
	void CreateEdge(){
		edge = Instantiate (Edge);
		edge.transform.position = (new Vector3 (MaxX + MinX, MaxY + MinY, MaxZ + MinZ)) / 2f;
		edge.transform.localScale = new Vector3 (MaxX - MinX, MaxY - MinY, MaxZ - MinZ);
	}

	void GetEdge(Item node){
		if (node.startNode.xcoord > MaxX) {
			MaxX = (float)node.startNode.xcoord;
		}
		if (node.startNode.xcoord < MinX) {
			MinX = (float)node.startNode.xcoord;
		}
		if (node.startNode.ycoord > MaxY) {
			MaxY = (float)node.startNode.ycoord;
		}
		if (node.startNode.ycoord < MinY) {
			MinY = (float)node.startNode.ycoord;
		}
//		if (node.startNode.zcoord > Maxz) {
//			Maxz = node.startNode.zcoord;
//		}
		if (node.startNode.zcoord < MinZ) {
			MinZ = (float)node.startNode.zcoord;
		}

//		if (node.endNode.xcoord > MaxX) {
//			MaxX = node.endNode.xcoord;
//		}
//		if (node.endNode.xcoord < MinX) {
//			MinX = node.endNode.xcoord;
//		}
//		if (node.endNode.ycoord > MaxY) {
//			MaxY = node.endNode.ycoord;
//		}
//		if (node.endNode.ycoord < MinY) {
//			MinY = node.endNode.ycoord;
//		}
		if (node.endNode.zcoord > MaxZ) {
			MaxZ = (float)node.endNode.zcoord;
		}
//		if (node.endNode.zcoord < MinZ) {
//			MinZ = node.endNode.zcoord;
//		}
	}

	public GameObject GuideHouse;




	IEnumerator RotTo(string name){

		Transform t = edge.transform.Find (name);

		for (int i = 0; i < 40; i++) {
			ca.transform.position = Vector3.Lerp (ca.transform.position, t.position, 0.25f);
			ca.transform.rotation = Quaternion.Lerp (ca.transform.rotation, t.rotation, 0.25f);
			yield return new WaitForSeconds (0.02f);
		}

		ca.transform.position = t.transform.position;
		ca.transform.rotation = t.rotation;
	}

	//--------------meshRenderer List处理---------------------------------
	//--------------meshRenderer List处理---------------------------------
	List<MeshRenderer> meshRendererListAll;
	List<MeshRenderer> storyMeshRendererList;
	List<MeshRenderer> columnMeshRendererList;
	List<Vector3> meshInScreen;

	List<string> SelectColumnList;
	List<string> SelectStoryList;

	void resetStoryMeshRendererList(){
		storyMeshRendererList = new List<MeshRenderer> ();
		for (int i = 0; i < meshRendererListAll.Count; i++) {
			storyMeshRendererList.Add (meshRendererListAll [i]);
		}
	}

	void removeStoryMeshRenenderList(List<string> storyList){
		foreach (string story in storyList) {
			foreach (Item node in storyDict[story]) {
				storyMeshRendererList.Remove (guidMeshDict [node.guid]);
			}
		}
	}

	void resetColumnMeshRendererList(){
		storyMeshRendererList = new List<MeshRenderer> ();
		for (int i = 0; i < meshRendererListAll.Count; i++) {
			columnMeshRendererList.Add (meshRendererListAll [i]);
		}
	}

	void removeColumnMeshRenenderList(List<string> columnList){
		foreach (string column in columnList) {
			foreach (Item node in columnDict[column]) {
				columnMeshRendererList.Remove (guidMeshDict [node.guid]);
			}
		}
	}

	void addColumnMeshRenenderList(List<string> columnList){
		foreach (string column in columnList) {
			foreach (Item node in columnDict[column]) {
				columnMeshRendererList.Add (guidMeshDict [node.guid]);
			}
		}
	}


	//--------------------画图------------------------------------------------
	//--------------------画图------------------------------------------------
	public void BeginDraw()
	{
		if (isStoryGroup) {
			for (int i = 0; i < storyMeshRendererList.Count; i++) {
				Vector3 position = ca.WorldToScreenPoint( storyMeshRendererList[i].transform.position );
				meshInScreen [i] = position;
			}
		}

		if (isColumnGroup) {
			for (int i = 0; i < columnMeshRendererList.Count; i++) {
				Vector3 position = ca.WorldToScreenPoint( columnMeshRendererList[i].transform.position );
				meshInScreen [i] = position;
			}
		}

	}

	public void Drawing( Vector2 point1, Vector2 point2)
	{
		Vector3 p1 = Vector3.zero;
		Vector3 p2 = Vector3.zero;

		if ( point1.x > point2.x )
		{
			p1.x = point2.x;
			p2.x = point1.x;
		}
		else
		{
			p1.x = point1.x;
			p2.x = point2.x;
		}
		if ( point1.y > point2.y )
		{
			p1.y = point2.y;
			p2.y = point1.y;
		}
		else
		{
			p1.y = point1.y;
			p2.y = point2.y;
		}
		Color color = new Color (0, 0.3f, 0);
		if (isStoryGroup) {
			for (int i = 0; i < storyMeshRendererList.Count; i++) {
				Vector3 position = meshInScreen [i];
				if ( position.x > p1.x && position.y > p1.y 
					&& position.x < p2.x && position.y < p2.y )
				{
					storyMeshRendererList [i].material = selectMat;
				}

			}
		}

		if (isColumnGroup) {
			for (int i = 0; i < columnMeshRendererList.Count; i++) {
				Vector3 position = meshInScreen [i];
				if ( position.x > p1.x && position.y > p1.y 
					&& position.x < p2.x && position.y < p2.y )
				{
					columnMeshRendererList[i].material = selectMat;
				}

			}
		}
	}


	public void EndDraw(Vector2 point1, Vector2 point2)
	{
		Vector3 p1 = Vector3.zero;
		Vector3 p2 = Vector3.zero;

		if ( point1.x > point2.x )
		{
			p1.x = point2.x;
			p2.x = point1.x;
		}
		else
		{
			p1.x = point1.x;
			p2.x = point2.x;
		}
		if ( point1.y > point2.y )
		{
			p1.y = point2.y;
			p2.y = point1.y;
		}
		else
		{
			p1.y = point1.y;
			p2.y = point2.y;
		}
		if (isStoryGroup) {
			setSelectStory (p1, p2);
			setColorByStory (SelectStoryList);
		}
		if (isColumnGroup) {
			setSelectColumn (p1, p2);
			setColorByColumn (SelectColumnList);
		}
	}

	//-------------------------单击----------------------------------
	//-------------------------单击----------------------------------
	int maskNode = 1 << 10;
	public void OnClick(Vector3 pos){
		RaycastHit hit;
		if (Physics.Raycast (ca.ScreenPointToRay (Input.mousePosition), out hit, 1500f,maskNode)) {
//			Debug.Log (hit.transform.parent.name);
			if (isStoryGroup) {
				SelectStoryList = new List<string> ();
				string storyName = guidItemDict [hit.transform.parent.name].storyID;
				if (!SelectStoryList.Contains (storyName)&&storySelectCountDict[storyName]!=2) {
					SelectStoryList.Add (storyName);
				}
				setColorByStory (SelectStoryList);
			}

			if (isColumnGroup) {
				SelectColumnList = new List<string> ();
				string columnName = guidItemDict [hit.transform.parent.name].columnLineID;
				if (!SelectColumnList.Contains (columnName)&&columnSelectCountDict[columnName]!=2) {
					SelectColumnList.Add (columnName);
				}
				setColorByColumn (SelectColumnList);
			}

		}
	}

	//----------------选择 分类 处理---------------------------------
	//----------------选择 分类 处理---------------------------------

	void setSelectStory(Vector2 p1, Vector2 p2){
		SelectStoryList = new List<string> ();
		for (int i = 0; i < storyMeshRendererList.Count; i++) {
			Vector3 position = meshInScreen [i];
			if ( position.x > p1.x && position.y > p1.y 
				&& position.x < p2.x && position.y < p2.y )
			{
				string storyName = guidItemDict [storyMeshRendererList [i].transform.parent.name].storyID;
				if (!SelectStoryList.Contains (storyName)&&storySelectCountDict[storyName]!=2) {
					SelectStoryList.Add (storyName);
//					Debug.Log (storyName);
				}

			}

		}
	}

	void setSelectColumn(Vector2 p1, Vector2 p2){
		SelectColumnList = new List<string> ();
		for (int i = 0; i < columnMeshRendererList.Count; i++) {
			Vector3 position = meshInScreen [i];
			if ( position.x > p1.x && position.y > p1.y 
				&& position.x < p2.x && position.y < p2.y )
			{
				string columnName = guidItemDict [columnMeshRendererList [i].transform.parent.name].columnLineID;
				if (!SelectColumnList.Contains (columnName)) {
					SelectColumnList.Add (columnName);
				}
			}

		}
	}

	Color highLightColor=new Color (0, 58/255f, 181/255f);
	void setColorByColumn(List<string> columnList){
		foreach (string column in columnList) {
			int count = columnSelectCountDict [column];
			if (count == 0) {
				columnSelectCountDict [column] = 1;
			} else if(count == 1){
				columnSelectCountDict [column] = 0;
			}
		}

		foreach (string column in columnSelectCountDict.Keys) {
			foreach (Item item in columnDict[column]) {
				if (columnSelectCountDict [column] == 0) {
					guidMeshDict [item.guid].material = startMat;
				} else if (columnSelectCountDict [column] == 1){
					guidMeshDict [item.guid].material = checkMat;
				}

			}
		}
	}


	void setColorByStory(List<string> storyList){
		foreach (string story in storyList) {
			int count = storySelectCountDict [story];
			if (count == 0) {
				storySelectCountDict [story] = 1;
			} else if(count == 1){
				storySelectCountDict [story] = 0;
			}
		}

		foreach (string story in storySelectCountDict.Keys) {
			foreach (Item item in storyDict[story]) {
				if (storySelectCountDict [story] == 0) {
					guidMeshDict [item.guid].material = startMat;
				} else if (storySelectCountDict [story] == 1){
					guidMeshDict [item.guid].material = checkMat;
				}

			}
		}
	}

	//------------------控制摄像机---------------------------------------
	//------------------控制摄像机---------------------------------------
	public Camera ca;
	public Camera GuideCamera;
	float Speed = 2;

	public void GuideCameraHit(Vector3 viewPort){
		RaycastHit hit;
		if (Physics.Raycast (GuideCamera.ViewportPointToRay(viewPort), out hit, 100f)) {
			if (hit.collider.CompareTag ("Guide")) {
				StartCoroutine (RotTo (hit.collider.name));
			}
		}
	}

	void CtrlCamera(){
		

		float hor = Input.GetAxis ("Horizontal");
		float ver = Input.GetAxis ("Vertical");
		if (Input.GetMouseButton (0)) {
			if (edge != null) {
				ca.transform.RotateAround (edge.transform.position, edge.transform.forward, hor*Time.deltaTime*Speed);
				ca.transform.RotateAround (edge.transform.position, ca.transform.right, -ver*Time.deltaTime*Speed);
			}

		}
		float scr = Input.GetAxis ("Mouse ScrollWheel");
		if (scr!=0) {
			ca.orthographicSize -= scr * Time.deltaTime * 500f;
		}

		GuideCamera.transform.rotation = ca.transform.rotation;
		GuideHouse.transform.position = GuideCamera.transform.position + GuideCamera.transform.forward * 2;
//		if(edge!=null)
//			GuideCamera.transform.position = GuideHouse.transform.position+ (ca.transform.position - edge.transform.position).normalized * 2;
		drag ();
	}

	//---------------------------Croup 处理----------------------------------
	//---------------------------Croup 处理----------------------------------
	string curStoryGroupID;
	string curColumnGroupID;
	void addStoryGroup(string id){
		Debug.Log (id);
		curStoryGroupID = id;
		storyGroup.Add (id, new List<string> ());
	}

	void addColumnGroup(string id){
		Debug.Log (id);
		curColumnGroupID = id;
		columnGroup.Add (id, new List<string> ());
	}

	void initDesignGroup(DesignGroups dgs){
		storyGroup = new Dictionary<string, List<string>> ();
		foreach (StoryGroup sg in dgs.StoryGroupIDs) {
            Debug.Log(sg.GroupID);
			storyGroup.Add (sg.GroupID, sg.StoryIDs);
		}

		columnGroup = new Dictionary<string, List<string>> ();
		foreach (ColumnGroup cg in dgs.ColumnGroupIDs) {
            Debug.Log(cg.GroupID);
            columnGroup.Add (cg.GroupID, cg.ColumnIDs);
		}
	}

	void deleteStoryGroup(string id){
		if (!storyGroup.ContainsKey (id))
			return ;

		foreach (string story in storyGroup[id]) {
			foreach (Item item in storyDict[story]) {

//				guidMeshDict [item.guid].material.SetColor ("_EmissionColor", Color.black);
				guidMeshDict [item.guid].material = startMat;
				storyMeshRendererList.Add (guidMeshDict [item.guid]);
			}
			storySelectCountDict [story] = 0;
		}

		storyGroup.Remove (id);
	}

	void deleteColumnGroup(string id){
		if (!columnGroup.ContainsKey (id))
			return ;

		foreach (string column in columnGroup[id]) {
			foreach (Item item in columnDict[column]) {

//				guidMeshDict [item.guid].material.SetColor ("_EmissionColor", Color.black);
				guidMeshDict [item.guid].material = startMat;
				columnMeshRendererList.Add (guidMeshDict [item.guid]);
			}
			columnSelectCountDict [column] = 0;
		}

		columnGroup.Remove (id);
	}

	void onCheckGroupClick(){

		if (isStoryGroup) {
			foreach (string story in storyDict.Keys) {
				if (storySelectCountDict [story] == 1) {
					storyGroup [curStoryGroupID].Add (story);
					storySelectCountDict [story] = 2;
				}
			}
			setStoryGroupColor (curStoryGroupID);
			OnStoryCheck (curStoryGroupID);

			if (storyMeshRendererList.Count == 0) {
				OnStoryComplete ();
				Debug.Log ("StoryGroup Complete");
			}
		}

		if (isColumnGroup) {
			foreach (string column in columnDict.Keys) {
				if (columnSelectCountDict [column] == 1) {
					columnGroup [curColumnGroupID].Add (column);
					columnSelectCountDict [column] = 2;
				}
			}
			setColumnGroupColor (curColumnGroupID);
			OnColumnCheck (curColumnGroupID);

			if (columnMeshRendererList.Count == 0) {
				OnColumnComplete ();
				Debug.Log ("ColumnGroup Complete");
			}
		}

	}

	void setStoryGroupColor(string storyGpName){

		Material mat = new Material (startMat);
		Color temp = new Color (Random.Range (0, 1f), Random.Range (0, 1f), Random.Range (0, 1f));
		mat.color = temp;
		foreach (string story in storyGroup[storyGpName]) {
			foreach (Item item in storyDict[story]) {
				guidMeshDict [item.guid].material = mat;
			}
		}

		removeStoryMeshRenenderList (storyGroup [storyGpName]);
	}

	void setColumnGroupColor(string columnGpName){
		Material mat = new Material (startMat);
		Color temp = new Color (Random.Range (0, 1f), Random.Range (0, 1f), Random.Range (0, 1f));
		mat.color = temp;
		foreach (string column in columnGroup[columnGpName]) {
			foreach (Item item in columnDict[column]) {

				guidMeshDict [item.guid].material = mat;
			}
		}

		removeColumnMeshRenenderList (columnGroup [columnGpName]);
	}
	Material matShine;
	Color colorShine;
	Material MatShine{
		get{ return matShine;}
		set{
			if (value != null) {
				if (matShine != null)
					matShine.color = colorShine;
				colorShine = value.color;
			}
			else
				matShine.color = colorShine;
			matShine = value;
		}
	}
	void shineSingleGroup(string id){
		if (isStoryGroup) {
			if(storyGroup.ContainsKey(id))
				MatShine = guidMeshDict [storyDict [storyGroup [id] [0]] [0].guid].sharedMaterial;
		} else if (isColumnGroup) {
			if(columnGroup.ContainsKey(id))
				MatShine = guidMeshDict [columnDict [columnGroup [id] [0]] [0].guid].sharedMaterial;
		} else {
			DesignGroup dg = JsonMapper.ToObject<DesignGroup> (id);
			string storyGroupID = dg.StoryGroupID;
			string columnGroupID = dg.ColumnGroupID;
			foreach (string story in storyGroup[storyGroupID]) {
				bool isGet = false;
				foreach (Item item in storyDict[story]) {
					if (columnGroup [columnGroupID].Contains (item.columnLineID)) {
						MatShine = guidMeshDict [item.guid].sharedMaterial;
						isGet = true;
						break;
					}
				}
				if (isGet)
					break;
			}
		}
	}

	void showAllDesignGroup(){
		foreach (string sname in storyGroup.Keys) {
			foreach (string cname in columnGroup.Keys) {
				setDesignGroupColor (sname, cname);
			}
		}
	}

	void setDesignGroupColor(string storyGroupID,string columnGroupID){
		Material mat = new Material (startMat);
		Color temp = new Color (Random.Range (0, 1f), Random.Range (0, 1f), Random.Range (0, 1f));
		mat.color = temp;

		foreach (string story in storyGroup[storyGroupID]) {
			foreach (Item item in storyDict[story]) {
				if (columnGroup [columnGroupID].Contains (item.columnLineID)) {
					guidMeshDict [item.guid].material = mat;
				}
			}
		}
	}

	void deleteSingleDesignGroup(string storyGroupID,string columnGroupID){
		foreach (string story in storyGroup[storyGroupID]) {
			foreach (Item item in storyDict[story]) {
				if (columnGroup [columnGroupID].Contains (item.columnLineID)) {
					guidMeshDict [item.guid].material = startMat;
				}
			}
		}
	}

	//-------------拖拽--------------------------------
	//-------------拖拽--------------------------------
	Vector3 MyScreenPointToWorldPoint(Vector3 ScreenPoint,Transform target)
	{
		//1 得到物体在主相机的xx方向
		Vector3 dir = (target.position - Camera.main.transform.position);
		//2 计算投影 (计算单位向量上的法向量)
		Vector3 norVec= Vector3.Project(dir, Camera.main.transform.forward);
		//返回世界空间
		return ca.ViewportToWorldPoint
			(
				new Vector3(
					ScreenPoint.x/Screen.width,
					ScreenPoint.y/Screen.height,
					norVec.magnitude
				)
			);

	}

	int mask = 1 << 9;
	bool isDrag=false;
	Transform hitObj;
	Vector3 startPos;
	Vector3 endPos;
	Vector3 offset;
	void drag(){
		RaycastHit hit;
		if ( Input.GetMouseButtonDown (2) && Physics.Raycast (ca.ScreenPointToRay (Input.mousePosition), out hit, 1000,mask)) {
			isDrag = true;
			hitObj = hit.collider.transform;
			startPos = MyScreenPointToWorldPoint (Input.mousePosition, hitObj);
		}

		if (isDrag && Input.GetMouseButton (2)) {
			if (hitObj == null)
				return;
			endPos = MyScreenPointToWorldPoint (Input.mousePosition, hitObj);
			offset = endPos - startPos;
			ca.transform.position -= offset;
			startPos = MyScreenPointToWorldPoint (Input.mousePosition, hitObj);
		}

		if ( Input.GetMouseButtonUp (2)) {
			isDrag = false;
			hitObj = null;
		}
	}

	//---------------阶段转换----------------------
	//---------------阶段转换----------------------
	bool isStoryGroup=false;
	bool isColumnGroup=false;
	void changeStage(string name){
		switch (name) {
		case "StoryGroup":
			isStoryGroup = true;
			isColumnGroup = false;
			resetMeshColor ();
			foreach (string id in storySelectCountDict.Keys) {
				if (storySelectCountDict [id] == 1) {
					storySelectCountDict [id] = 0;
				}
			}

			foreach (var sg in storyGroup.Keys) {
				setStoryGroupColor (sg);
			}
			break;
		case "ColumnGroup":
			isStoryGroup = false;
			isColumnGroup = true;
			resetMeshColor ();
			foreach (string id in columnSelectCountDict.Keys) {
				if (columnSelectCountDict [id] == 1) {
					columnSelectCountDict [id] = 0;
				}
			}

			foreach (var cg in columnGroup.Keys) {
				setColumnGroupColor (cg);
			}
			break;
		case "DesignGroup":
			isStoryGroup = false;
			isColumnGroup = false;
			resetMeshColor ();
			break;
			default:
				break;
		}
	}

	void resetMeshColor(){
		foreach (var mesh in meshRendererListAll) {
			mesh.material = startMat;
		}
	}

	//--------------------材质闪耀-----------------------------------------
	float shineTimer=0;
	Color GoldColor=new Color(1,1,0);
	void shineMatUpdate(){
		if (MatShine == null) {
			return;
		}
		shineTimer += Time.deltaTime;
		if (shineTimer < 0.8f) {
			matShine.color = Color.Lerp (matShine.color,GoldColor , 3f * Time.deltaTime);
//			Debug.Log (matShine.color);
		} else if (shineTimer < 1.6f) {
			matShine.color = Color.Lerp (matShine.color, colorShine, 3f * Time.deltaTime);
		}
		if (shineTimer > 1.6f) {
			shineTimer = 0;
		}

	}

	//--------------------Contact to WEB------------------------------
	//--------------------Contact to WEB------------------------------

	void OnStoryComplete(){
		Application.ExternalCall ("UnityCallWeb", "StoryComplete", JsonMapper.ToJson(storyGroup));
	}

	void OnColumnComplete(){
		Application.ExternalCall ("UnityCallWeb", "ColumnComplete", JsonMapper.ToJson(columnGroup));
	}

	void OnStoryCheck(string sgID){
		StoryGroup sg = new StoryGroup ();
		sg.GroupID = sgID;
		sg.StoryIDs = storyGroup [sgID];
		Application.ExternalCall ("UnityCallWeb", "StoryCheck", JsonMapper.ToJson(sg));
	}

	void OnColumnCheck(string cgID){
		ColumnGroup cg = new ColumnGroup ();
		cg.GroupID = cgID;
		cg.ColumnIDs = columnGroup [cgID];

		Application.ExternalCall ("UnityCallWeb", "ColumnCheck", JsonMapper.ToJson(cg));
	}
	void OnDataGetComplete(){
		Application.ExternalCall ("UnityCallWeb", "DataComplete", "");
	}

	public void NewColumnGroup(string id){
		addColumnGroup (id);
	}

	public void NewStoryGroup(string id){
		addStoryGroup (id);
	}
	//切换步骤：StoryGroup，ColumnGroup，DesignGroup
	public void ChangeStage(string stage){
		changeStage (stage);
	}

	public void DeleteStoryGroup(string id){
		deleteStoryGroup (id);
	}

	public void DeleteColumnGroup(string id){
		deleteColumnGroup (id);
	}

	//闪耀单个group
	public void ShineSingleGroup(string id){
		shineSingleGroup (id);
	}

	public void CancelShine(string id){
		MatShine = null;
	}

	//重置颜色
	public void ResetAllColor(string a){
		resetMeshColor ();
	}
	//展示单个DG
	public void ShowDesignGroup(string json){
		DesignGroup dg = JsonMapper.ToObject<DesignGroup> (json);
		setDesignGroupColor (dg.StoryGroupID, dg.ColumnGroupID);
	}
	//删除单个DG
	public void DeleteDesignGroup(string json){
		DesignGroup dg = JsonMapper.ToObject<DesignGroup> (json);
		deleteSingleDesignGroup (dg.StoryGroupID, dg.ColumnGroupID);
	}

	//展示所有DG
	public void ShowAllDesignGroup(string json){
		showAllDesignGroup ();
	}

	//初始化DG,以前版本
	public void InitDesignGroup(string json){
		DesignGroups dgs = JsonMapper.ToObject<DesignGroups> (json);
		initDesignGroup (dgs);
	}
}

public class StoryGroup{
	public string GroupID;
	public List<string> StoryIDs;
}

public class ColumnGroup{
	public string GroupID;
	public List<string> ColumnIDs;
}

public class DesignGroup{
	public string StoryGroupID;
	public string ColumnGroupID;
}

public class DesignGroups{
	public List<StoryGroup> StoryGroupIDs;
	public List<ColumnGroup> ColumnGroupIDs;
}


