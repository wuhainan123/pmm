﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BuildAxis : MonoBehaviour {

	public GameObject Axis;
	public GameObject Cone;
	public GameObject Text;
	public GameObject AxisLine;


	// Use this for initialization
	void Start () {
		
	}
	
	public void SetAxis(Transform edge ,float minZ){
		Vector3 xRot = new Vector3 (0, 0, -90);
		Vector3 zRot = new Vector3 (90, 0, 0);

		GameObject axisx = Instantiate (Axis);
		axisx.transform.position = Vector3.zero;
		axisx.transform.eulerAngles = xRot;
		axisx.transform.localScale = new Vector3 (1,edge.localScale.x/2.0f, 1);
		axisx.transform.GetComponentInChildren<MeshRenderer> ().material.color = Color.red;

		GameObject conex = Instantiate (Cone);
		conex.transform.position = axisx.transform.Find ("Pos").transform.position;
		conex.transform.localEulerAngles = xRot;
		conex.transform.GetComponentInChildren<MeshRenderer> ().material.color = Color.red;

		//--------------
		GameObject axisy = Instantiate (Axis);
		axisy.transform.position = Vector3.zero;
		axisy.transform.eulerAngles = Vector3.zero;
		axisy.transform.localScale = new Vector3 (1,edge.localScale.y/2.0f,  1);
		axisy.transform.GetComponentInChildren<MeshRenderer> ().material.color = Color.green;

		GameObject coney = Instantiate (Cone);
		coney.transform.position = axisy.transform.Find ("Pos").transform.position;
		coney.transform.localEulerAngles = Vector3.zero;
		coney.transform.GetComponentInChildren<MeshRenderer> ().material.color = Color.green;

		//-------------
		GameObject axisz = Instantiate (Axis);
		axisz.transform.position = new Vector3 (0, 0, minZ);
		axisz.transform.eulerAngles = zRot;
		axisz.transform.localScale = new Vector3 (1,edge.localScale.z/2.0f,  1);
		axisz.transform.GetComponentInChildren<MeshRenderer> ().material.color = Color.blue;

		GameObject conez = Instantiate (Cone);
		conez.transform.position = axisz.transform.Find ("Pos").transform.position;
		conez.transform.localEulerAngles = zRot;
		conez.transform.GetComponentInChildren<MeshRenderer> ().material.color = Color.blue;


		Transform camera=GameObject.Find("GuideCamera").transform;
		Transform textParent = (new GameObject ()).transform;
		List<Storie> sts = GameObject.FindObjectOfType<NetManager> ().msg.stories;

		GameObject asixLine = Instantiate (AxisLine);
		asixLine.transform.eulerAngles = xRot;
		asixLine.transform.localScale = new Vector3 (1,edge.localScale.x/2.0f, 1);
		foreach (Storie st in sts) {
			GameObject g = Instantiate (Text);
			g.transform.position = new Vector3 (0, 0, (float)st.elevation);
			g.GetComponent<TextMesh> ().text = st.storyName+"  ";
			TextLookAt ta= g.AddComponent<TextLookAt> ();
			ta.ca = camera;
			g.transform.SetParent (textParent);

			GameObject temp = Instantiate (asixLine);
			temp.transform.position = new Vector3 (0, 0, (float)st.elevation);
			temp.transform.localScale = new Vector3 (0.2f, temp.transform.localScale.y, 0.2f);
		}
	}
}
