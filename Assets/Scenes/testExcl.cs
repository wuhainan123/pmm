﻿using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.IO;
using Excel;
using ICSharpCode.SharpZipLib;
using UnityEngine;

public class testExcl : MonoBehaviour {

    public GameObject LineObj;
    public GameObject NodeObj;
    NodeLine[] nodeLines;
    NodePoint[] nodePoints;

    Dictionary<int,List<Vector3>> linesDict;
    Dictionary<string, List<GameObject>> pointsDict;
    // Use this for initialization
    void Start () {
        creatLines();
        creatPoints();
    }

    void Update() {

    }

    void ShowLoadCase(string loadCase) {
        if (pointsDict.ContainsKey(loadCase)) {
            foreach (GameObject g in pointsDict[loadCase]) {
                g.SetActive(true);
            }
        }
    }

    void HideLoadCase(string loadCase)
    {
        if (pointsDict.ContainsKey(loadCase))
        {
            foreach (GameObject g in pointsDict[loadCase])
            {
                g.SetActive(false);
            }
        }
    }

    void creatPoints() {
        pointsDict = new Dictionary<string, List<GameObject>>(); 
        nodePoints = CreateNodePointArrayWithExcel(Application.dataPath + "/Excels/forcePMMDataTest.xlsx");
        foreach (NodePoint node in nodePoints)
        {
            GameObject point = Instantiate(NodeObj);
            point.transform.position = new Vector3(node.X, node.Y, node.Z / 2.5f) / 100f;
            if (!pointsDict.ContainsKey(node.LoadCase))
            {
                List<GameObject> list = new List<GameObject>();
                pointsDict.Add(node.LoadCase, list);
            }

            if (!node.Remarks.Contains("Passed")) {
                point.GetComponent<MeshRenderer>().material.color = Color.red;
            }
            pointsDict[node.LoadCase].Add(point);
        }

    }

    NodePoint[] CreateNodePointArrayWithExcel(string filePath)
    {
        //获得表数据
        int columnNum = 0, rowNum = 0;
        DataRowCollection collect = ReadExcel(filePath, ref columnNum, ref rowNum);

        //根据excel的定义，第二行开始才是数据
        NodePoint[] array = new NodePoint[rowNum - 1];
        for (int i = 1; i < rowNum; i++)
        {
            NodePoint item = new NodePoint();
            //解析每列的数据
            item.LoadCase = collect[i][0].ToString();
            item.Z = float.Parse(collect[i][1].ToString());
            item.X = float.Parse(collect[i][2].ToString());
            item.Y = float.Parse(collect[i][3].ToString());
            item.Remarks = collect[i][4].ToString();
            array[i - 1] = item;
        }
        Debug.Log(array.Length);
        return array;
    }

    //-------------------------------线-------------------------------
    //-------------------------------线-------------------------------

    void creatLines()
    {
        linesDict = new Dictionary<int, List<Vector3>>();
        nodeLines = CreateNodeLineArrayWithExcel(Application.dataPath + "/Excels/sectionPMMDataTest.xlsx");
        foreach (NodeLine node in nodeLines)
        {
            if (linesDict.ContainsKey(node.SEQUENCE))
            {
                linesDict[node.SEQUENCE].Add(new Vector3(node.X, node.Y, node.Z / 2.5f) / 100f);
            }
            else
            {
                List<Vector3> list = new List<Vector3>();
                linesDict.Add(node.SEQUENCE, list);
                linesDict[node.SEQUENCE].Add(new Vector3(node.X, node.Y, node.Z / 2.5f) / 100f);
            }
        }

        foreach (int sq in linesDict.Keys)
        {
            GameObject temp = Instantiate(LineObj);
            LineRenderer re = temp.GetComponent<LineRenderer>();
            re.positionCount = linesDict[sq].Count;
            re.SetPositions(linesDict[sq].ToArray());
        }
    }


    NodeLine[] CreateNodeLineArrayWithExcel(string filePath)
    {
        //获得表数据
        int columnNum = 0, rowNum = 0;
        DataRowCollection collect = ReadExcel(filePath, ref columnNum, ref rowNum);

        //根据excel的定义，第二行开始才是数据
        NodeLine[] array = new NodeLine[rowNum - 1];
        for (int i = 1; i < rowNum; i++)
        {
            NodeLine item = new NodeLine();
            //解析每列的数据
            item.Z = float.Parse(collect[i][0].ToString());
            item.X = float.Parse(collect[i][1].ToString());
            item.Y = float.Parse(collect[i][2].ToString());
            item.SEQUENCE = int.Parse(collect[i][3].ToString());
            array[i - 1] = item;
        }
        Debug.Log(array.Length);
        return array;
    }

    /// <summary>
    /// 读取excel文件内容
    /// </summary>
    /// <param name="filePath">文件路径</param>
    /// <param name="columnNum">行数</param>
    /// <param name="rowNum">列数</param>
    /// <returns></returns>
     DataRowCollection ReadExcel(string filePath, ref int columnNum, ref int rowNum)
    {
        FileStream stream = File.Open(filePath, FileMode.Open, FileAccess.Read, FileShare.Read);
        IExcelDataReader excelReader = ExcelReaderFactory.CreateOpenXmlReader(stream);

        DataSet result = excelReader.AsDataSet();
        //Tables[0] 下标0表示excel文件中第一张表的数据
        columnNum = result.Tables[0].Columns.Count;
        rowNum = result.Tables[0].Rows.Count;
        return result.Tables[0].Rows;
    }
}

[System.Serializable]
public class NodeLine {
    public float Z;
    public float X;
    public float Y;
    public int SEQUENCE;
}

public class NodePoint {
    public string LoadCase;
    public float Z;
    public float X;
    public float Y;
    public string Remarks;

}
