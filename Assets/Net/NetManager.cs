﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Text;
using System.Net;
using System.IO;
using System;
using LitJson;

public class NetManager : MonoBehaviour {

	public BuildingData msg;

	// Use this for initialization
	void Start () {

//		Debug.Log(PostWebRequest("http://192.168.1.157:57970/api/elements/getallelements", "" ));
//		StartCoroutine(GetPhotos());

//		msg=JsonMapper.ToObject<NetMsg>(ReadTxt());
//		GameObject.FindObjectOfType<BuildCube> ().StartBuild (msg.data);

		Application.ExternalCall ("UnityCallWeb", 1, "");
	}


	void Update(){
		if (Input.GetKeyDown (KeyCode.I)) {
			msg=JsonMapper.ToObject<BuildingData>(ReadTxt("demoElements.txt"));
			Debug.Log (msg.columnLines.Count);
			Debug.Log (msg.stories.Count);
			GameObject.FindObjectOfType<BuildCube> ().StartBuild (msg.elementViewMobels);
		}

        //if (Input.GetKeyDown(KeyCode.G))
        //{
        //    string aaa = ReadTxt("testDesignGroupData.txt");
        //    Debug.Log(aaa);
        //    GameObject.FindObjectOfType<BuildCube>().InitDesignGroup(aaa);
        //    GameObject.FindObjectOfType<BuildCube>().ShowAllDesignGroup("");
        //}
    }

	public void StartInit(string json){
		msg=JsonMapper.ToObject<BuildingData>(json);

		GameObject.FindObjectOfType<BuildCube> ().StartBuild (msg.elementViewMobels);
	}
	
	private string PostWebRequest(string postUrl, string paramData)
	{
		// 把字符串转换为bype数组
		byte[] bytes = Encoding.GetEncoding("gb2312").GetBytes(paramData);

		HttpWebRequest webReq = (HttpWebRequest) WebRequest.Create(new Uri(postUrl));
		webReq.Method = "POST";
		webReq.ContentType = "application/x-www-form-urlencoded;charset=gb2312";
		webReq.ContentLength = bytes.Length;
		using (Stream newStream = webReq.GetRequestStream())
		{
			newStream.Write(bytes, 0, bytes.Length);
		}
		using (WebResponse res = webReq.GetResponse())
		{
			//在这里对接收到的页面内容进行处理
			Stream responseStream = res.GetResponseStream();
			StreamReader streamReader = new StreamReader(responseStream, Encoding.GetEncoding("UTF-8"));
			string str = streamReader.ReadToEnd();
			streamReader.Close();
			responseStream.Close();
			//返回：服务器响应流 
			return str;
		}
	}

	IEnumerator GetPhotos()
	{
		WWWForm form = new WWWForm();
		form.AddField("userid","ABC");
		WWW w = new WWW ("http://192.168.1.157:8001/api/elements/getallelements", form);
		yield return w;
		if (w.error != null) {
			Debug.Log (w.error);
		} else {
//			msg = JsonUtility.FromJson<NetMsg> (w.text);
			msg=JsonMapper.ToObject<BuildingData>(w.text);

//			Debug.Log (w.text);
			WriteTxt(w.text);

			GameObject.FindObjectOfType<BuildCube> ().StartBuild (msg.elementViewMobels);


//			WWW
//			Debug.Log (msg.result);
//			Debug.Log (msg.data.Length);
		}
//		Debug.Log(w.text);

	}

	void WriteTxt(string value){
		using (FileStream fs = new FileStream(Application.dataPath+"/file.txt", FileMode.OpenOrCreate, FileAccess.Write))
		{
			//写二进制
			using (StreamWriter sw = new StreamWriter(fs))
			{
				sw.Write (value);
			}
		}
	}

	string ReadTxt(string fileName){
		using (FileStream fs = new FileStream(Application.streamingAssetsPath+"/"+fileName, FileMode.OpenOrCreate, FileAccess.Read))
		{
			//写二进制
			using (StreamReader sw = new StreamReader(fs))
			{
				return sw.ReadToEnd ();
			}
		}
	}
		
}

public class BuildingData{
//	public bool result;
//	public string title;
//	public string content;
//	public string[] resultList;
//	public string extendData;
	public Item[] elementViewMobels;
	public List<Storie> stories;
	public List<ColumnLine> columnLines;
}

public class Item{
	public string columnLineID;//列
	public string elementName;//单元名字
	public string storyName;
	public string columnLineName;
	public Node endNode;
	public string guid;
//	public string sectionName;
	public Node startNode;
	public string storyID;//层

	public float GetHeight(){
		return (float)(endNode.zcoord - startNode.zcoord);
	}
}

public class Node{
//	public string nodeId;
//	public string nodeName;
	public double xcoord;
	public double ycoord;
	public double zcoord;
	public Vector3 GetVec3(){
		return new Vector3 ((float)xcoord, (float)ycoord, (float)zcoord);
	}
}

public class Storie{
	public string storyId;
	public string storyName;
	public string storyGroupId;
	public double height;
	public double elevation;
}

public class ColumnLine{
	public string columnLineId;
	public string columnLineName;
	public string columnTagId;
}

