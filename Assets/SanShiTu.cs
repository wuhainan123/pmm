﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class SanShiTu : MonoBehaviour ,IPointerClickHandler{

	RectTransform rect;
	BuildCube buildCube;

	void Start(){
		rect = GetComponent<RectTransform> ();
		buildCube = GameObject.FindObjectOfType<BuildCube> ();
	}
	public void OnPointerClick (PointerEventData eventData)
	{
		
		Vector3 viewPort = new Vector3 (1 - (Screen.width - Input.mousePosition.x) / rect.rect.width, 1 - (Screen.height - Input.mousePosition.y) / rect.rect.height, 0.2f);
		buildCube.GuideCameraHit (viewPort);
	}
		
}
